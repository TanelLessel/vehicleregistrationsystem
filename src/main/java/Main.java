import service.BuyerService;
import service.SellerService;
import utils.DataGenerator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DataGenerator dataGenerator = new DataGenerator();
        dataGenerator.generateCarBrands();


        Scanner scanner = new Scanner(System.in);

        BuyerService buyerService = new BuyerService();
        SellerService sellerService = new SellerService();


        String userInput = "";

        while (!userInput.equals("3")) {
            System.out.println("Welcome to ARK!"
                    + "\n 1. Buy Car"
                    + "\n 2. Sell Car"
                    + "\n 3. Quit");
            userInput = scanner.nextLine();

            switch (userInput) {
                case "1":
                    buyerService.buyCar();
                    break;
                case "2":
                    sellerService.addCar();
                    break;
                default:
                    System.exit(0);
                    break;
            }
        }

    }
}

