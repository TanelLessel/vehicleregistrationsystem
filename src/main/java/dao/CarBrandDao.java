package dao;

import entity.CarBrand;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.Hibernate4Util;

import java.util.List;

public class CarBrandDao {
    public void saveCarBrand(CarBrand carBrand) {
        if (getCarBrandByName(carBrand.getName()) == null) {
            Transaction transaction = null;
            try (Session session = Hibernate4Util.getSessionFactory().getCurrentSession()) {
                transaction = session.beginTransaction();
                session.save(carBrand);
                transaction.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public List<CarBrand> getAllCarBrands() {
        try (Session session = Hibernate4Util.getSessionFactory().openSession()) {
            return session.createQuery("from CarBrand", CarBrand.class).list();
        }
    }

    public CarBrand getCarBrandByName(String name) {
        try (Session session = Hibernate4Util.getSessionFactory().openSession()) {
            Query<CarBrand> query = session.createQuery("from CarBrand where name= :name", CarBrand.class);
            query.setParameter("name", name);
            List<CarBrand> carBrands = query.list();
            return carBrands.size() > 0 ? query.list().get(0) : null;
        }
    }

    public CarBrand getCarBrandById(Long id) {
        try (Session session = Hibernate4Util.getSessionFactory().openSession()) {
            Query<CarBrand> query = session.createQuery("from CarBrand where id= :id", CarBrand.class);
            query.setParameter("id", id);
            List<CarBrand> carBrands = query.list();
            return carBrands.size() > 0 ? query.list().get(0) : null;
        }
    }

    public void saveBulkCarBrands(List<CarBrand> carBrands) {
        for (CarBrand a : carBrands) {
            saveCarBrand(a);
        }
    }
}
