package dao;


import entity.Car;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.Hibernate4Util;

import java.util.List;

public class CarDao {
    public void saveCar(Car car) {
        Transaction transaction = null;
        try (Session session = Hibernate4Util.getSessionFactory().getCurrentSession()) {
            transaction = session.beginTransaction();
            session.save(car);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void buyCar(Car car, String firstName, String lastName, String sex, int age, String socialSecurityNumber) {
        Transaction transaction = null;
        try (Session session = Hibernate4Util.getSessionFactory().getCurrentSession()) {
            transaction = session.beginTransaction();

            car = session.get(Car.class, car.getId());
            car.setFirstName(firstName);
            car.setLastName(lastName);
            car.setSex(sex);
            car.setAge(age);
            car.setSocialSecurityNumber(socialSecurityNumber);
            car.setSell(false);

            session.save(car);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Car> getAllCars() {
        try (Session session = Hibernate4Util.getSessionFactory().openSession()) {
            return session.createQuery("from Car", Car.class).list();
        }
    }

    public List<Car> getCarsForSale() {
        try (Session session = Hibernate4Util.getSessionFactory().openSession()) {
            return session.createQuery("from Car where sell is true order by price", Car.class).list();
        }
    }

    public Car getCarById(Long id) {
        try (Session session = Hibernate4Util.getSessionFactory().openSession()) {
            Query<Car> query = session.createQuery("from Car where id= :id", Car.class);
            query.setParameter("id", id);
            List<Car> cars = query.list();
            return cars.size() > 0 ? query.list().get(0) : null;
        }
    }
}