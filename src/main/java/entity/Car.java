package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String sex;
    private int age;
    private String socialSecurityNumber;

    private int price;

    private boolean sell;

    @OneToOne(cascade = CascadeType.ALL)
    private CarBrand carBrand;


}


