package service;

import dao.CarBrandDao;
import dao.CarDao;
import entity.Car;
import entity.CarBrand;

import java.util.List;
import java.util.Scanner;

public class BuyerService {

    public void buyCar() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Cars for sale:");
        for (Car car : new CarDao().getCarsForSale()) {
            System.out.println(car.getId() + ". " + car.getCarBrand().getName() + " - " + car.getPrice() + " €");
        }


        System.out.println("Please select a car:");
        long carId = Long.parseLong(scanner.nextLine());

        CarDao carDao = new CarDao();

        Car car = carDao.getCarById(carId);

        if (car == null || !car.isSell()) {
            System.out.println("Car not found");
            return;
        }

        System.out.println("Please enter payment amount");
        int payment = Integer.parseInt(scanner.nextLine());

        if (payment < car.getPrice()) {
            System.out.println("Sorry, not enough");
            return;
        }

        System.out.println("Please enter First Name");
        String firstName = scanner.nextLine();
        System.out.println("Please enter Last Name");
        String lastName = scanner.nextLine();
        System.out.println("Please enter Sex");
        String sex = scanner.nextLine();
        System.out.println("Please enter Age");
        int age = Integer.parseInt(scanner.nextLine());
        System.out.println("Please enter Social Security Number");
        String socialSecurityNumber = scanner.nextLine();

        carDao.buyCar(car, firstName, lastName, sex, age, socialSecurityNumber);

        System.out.println("Congratulations, you've purchased the car and it's registered to your name!");
    }
}

