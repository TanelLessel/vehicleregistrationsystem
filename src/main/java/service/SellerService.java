package service;

import dao.CarBrandDao;
import dao.CarDao;
import entity.Car;

import java.util.Objects;
import java.util.Scanner;

public class SellerService {
    public void addCar() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter First Name");
        String firstName = scanner.nextLine();
        System.out.println("Please enter Last Name");
        String lastName = scanner.nextLine();
        System.out.println("Please enter Sex");
        String sex = scanner.nextLine();
        System.out.println("Please enter Age");
        int age = Integer.parseInt(scanner.nextLine());
        System.out.println("Please enter Social Security Number");
        String socialSecurityNumber = scanner.nextLine();
        System.out.println("Please select a car brand:");
        new CarBrandDao().getAllCarBrands().forEach(carBrand -> System.out.println(carBrand.getId() + ". " + carBrand.getName()));
        Long carBrandId = Long.parseLong(scanner.nextLine());

        System.out.println("Please enter price");
        int price = Integer.parseInt(scanner.nextLine());

        Car car = new Car(null, firstName, lastName, sex, age, socialSecurityNumber, price, true, new CarBrandDao().getCarBrandById(carBrandId));
        new CarDao().saveCar(car);
    }
}
