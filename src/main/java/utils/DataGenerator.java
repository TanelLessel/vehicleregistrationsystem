package utils;

import dao.CarBrandDao;
import entity.CarBrand;

import java.util.Arrays;
import java.util.List;

public class DataGenerator {
    public void generateCarBrands() {
        List<CarBrand> carBrandList = Arrays.asList(new CarBrand(null, "MERCEDES"), new CarBrand(null, "BMW"));
        new CarBrandDao().saveBulkCarBrands(carBrandList);
    }
}